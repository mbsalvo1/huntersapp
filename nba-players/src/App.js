import "bootstrap/dist/css/bootstrap.min.css";
// import { Analytics } from '@vercel/analytics/react';
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Home from "./components/Home";
import NavBar from "./components/NavbarComp";
import NavSearch from "./components/NavSearch";
import PlayerDetail from "./components/PlayerDetail";
import SearchLand from "./components/searchLand";

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      {/* <Analytics/> */}
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/search" element={<SearchLand />} />
        <Route path="/search/:navSearch" element={<NavSearch />} />
        <Route path="/player/:detail" element={<PlayerDetail />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
