import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { Button } from "react-bootstrap";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function NavBar() {
  const [navSearch, setNavSearch] = useState("");

  const navigate = useNavigate();

  const handleInputChange = (event) => {
    setNavSearch(event.target.value);
  };

  const submitHandler = () => {
    if (navSearch.length >= 1) {
      navigate(`/search/${navSearch}`);
    }
  };

  return (
    <Navbar bg="light" expand="lg">
      <Container>
        {/* <Navbar.Brand href="/">Hunters NavBar</Navbar.Brand> */}
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="https://www.nba.com/games">NBA Games Today</Nav.Link>
            <Nav.Link href="https://www.nfl.com/schedules/">NFL Games This Week</Nav.Link>
          </Nav>
        </Navbar.Collapse>
        <form
          onSubmit={(event) => {
            event.preventDefault();
          }}
          id="search-form"
        >
          <div className="input-group mb-1 p-2">
            <input
              onChange={handleInputChange}
              className="form-control"
              name="searchPlayers"
              id="searchPlayers"
              type="search"
              placeholder="Search players"
            />
            <Button type="submit" variant="dark" onClick={submitHandler}>
              Submit
            </Button>
          </div>
        </form>
      </Container>
    </Navbar>
  );
}

export default NavBar;
