import { Container, Card } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Loading from "./Loading";
import { Link } from "react-router-dom";

function NavSearch() {
  const [player, setPlayer] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  let { navSearch } = useParams();

  useEffect(() => {
    const fetchPlayers = async () => {
      setIsLoading(true);
      const url = `https://www.thesportsdb.com/api/v1/json/3/searchplayers.php?p=${navSearch}`;
      const result = await fetch(url);
      const data = await result.json();
      setPlayer(data.player);
      setIsLoading(false);
    };

    fetchPlayers();
  }, [navSearch]);
  console.log(player)

  if (player) {
    return (
      <Container
        style={{
          backgroundImage: `url(https://images.pexels.com/photos/2277981/pexels-photo-2277981.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1d)`,
          backgroundRepeat: "no-repeat",
          backgroundPositionY: "center",
          backgroundSize: "cover",
          height: "100vh",
        }}
        fluid="md"
        className="mt-3 fade-in"
      >
        <div>
          <b align="left" className="text-white">
            {player.length} Results for {navSearch}
          </b>
        </div>
        {isLoading ? 
        <Loading/>
        :
        <Container>
          <center className="mt-3 scroll-container">
            {player.map((row) => {
              return (
                <Link to={`/player/${row.idPlayer}`}
                className="text-black"
                style={{ textDecoration: "none" }}
                >
                <Card
                  className="mb-3"
                  style={{ width: "15rem" }}
                  key={row.idPlayer}
                >
                  <Card.Header align="center" className="fw-bold">
                    {row.strPlayer}
                  </Card.Header>
                  <Card.Body>
                    {row.strThumb ? (
                      <Card.Img variant="bottom" src={row.strThumb} />
                    ) : (
                      <p>No image found... coming soon</p>
                    )}
                  </Card.Body>
                  <Card.Text>
                    Team : {row.strTeam}
                    <br></br>
                    Position: {row.strPosition}
                    <br></br>
                    Player Height: {row.strHeight}
                    <br></br>
                    Player Weight: {row.strWeight}
                  </Card.Text>
                </Card>
                </Link>
              );
            })}
          </center>
        </Container>
  }
        
      </Container>
    );
  } else {
    return (
      <Container
        style={{
          backgroundImage: `url(https://images.pexels.com/photos/2277981/pexels-photo-2277981.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1d)`,
          backgroundRepeat: "no-repeat",
          backgroundPositionY: "center",
          backgroundSize: "cover",
          height: "100vh",
        }}
        fluid="md"
        className="mt-3 fade-in"
      >
        <center>
          <h1 className="text-white">0 results. Please refine your search.</h1>
        </center>
      </Container>
    );
  }
}

export default NavSearch;
