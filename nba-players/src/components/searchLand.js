import { Container, Card } from "react-bootstrap";
import { useEffect, useState } from "react";


function SearchLand() {
  const [player, setPlayer] = useState([]);


  useEffect(() => {
    const fetchPlayers = async () => {
      const url = `https://www.thesportsdb.com/api/v1/json/3/searchplayers.php?p=messi`;
      const result = await fetch(url);
      const data = await result.json();
      setPlayer(data.player);
    }
    fetchPlayers();
  }, []);

  if (player) {
    return (
      <Container
        style={{
          backgroundImage: `url(https://images.pexels.com/photos/2277981/pexels-photo-2277981.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1d)`,
          backgroundRepeat: "no-repeat",
          backgroundPositionY: "center",
          backgroundSize: "cover",
          height: "100vh",
        }}
        fluid="md"
        className="mt-3 fade-in"
      >
       
        <div>
          <h1 align="left" className="text-white">
            Results: {player.length}
          </h1>
        </div>
        
        <Container>
          <center>
            {player.map((row) => {
              return (
                <Card
                  className="mb-3"
                  style={{ width: "15rem" }}
                  key={row.idPlayer}
                >
                  <Card.Header align="center" className="fw-bold">
                    {row.strPlayer}
                  </Card.Header>
                  <Card.Body>
                    {row.strThumb ? (
                      <Card.Img variant="bottom" src={row.strThumb} />
                    ) : (
                      <p>No image found... coming soon</p>
                    )}
                  </Card.Body>
                  <Card.Text>
                    Team : {row.strTeam}
                    <br></br>
                    Player Height: {row.strHeight}
                    <br></br>
                    Player Weight: {row.strWeight}
                  </Card.Text>
                </Card>
              );
            })}
          </center>
        </Container>
      </Container>
    );
  } else {
    return (
      <center>
        <h1 className="text-black">0 results. Please refine your search.</h1>
      </center>
    );
  }
}

export default SearchLand;
