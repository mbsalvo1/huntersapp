import React from 'react'

function Loading() {

  const balls = [
    "/baseball.png",
    "/bball.png",
    "/football.png",
    "/tennis.png",
    "/puck.png",
    "/soccer.png",
  ]

  const randomBall = balls[Math.floor(Math.random() * balls.length)]

  return (
    <div align="center">
        <img className="ball" src={randomBall} alt= "Ball" width="100rem"/>
    </div>
  )
}

export default Loading