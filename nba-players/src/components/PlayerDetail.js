import { Container, Card } from "react-bootstrap";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Loading from "./Loading";

function PlayerDetail() {
  const [player, setPlayer] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const { detail } = useParams();
  console.log("detail",detail)

  useEffect(() => {
    const fetchPlayers = async () => {
      setIsLoading(true);
      const url = `https://www.thesportsdb.com/api/v1/json/3/lookupplayer.php?id=${detail}`;
      const result = await fetch(url);
      const data = await result.json();
      console.log(data.players)
      setPlayer(data.players[0]);
      setIsLoading(false);
    };

    fetchPlayers();
  }, [detail]);

  console.log(player)

    return (
      <Container
        style={{
          backgroundImage: `url(https://images.pexels.com/photos/2277981/pexels-photo-2277981.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1d)`,
          backgroundRepeat: "no-repeat",
          backgroundPositionY: "center",
          backgroundSize: "cover",
          height: "100vh",
        }}
        fluid="md"
        className="mt-3 fade-in"
      >
        <div>
        </div>
        {isLoading ? 
        <Loading/>
        :
        <Container fluid>
            <div>
          <center className="mt-3 scroll-container">
                <Card
                  className="mt-5"
                  style={{ width: "20rem" }}
                >
                  <Card.Header align="center" className="fw-bold">
                    {player.strPlayer}
                  </Card.Header>
                  <Card.Body>
                    {player.strThumb ? (
                      <Card.Img variant="bottom" src={player.strThumb} />
                    ) : (
                      <p>No image found... coming soon</p>
                    )}
                  </Card.Body>
                  <Card.Text>
                    Team : {player.strTeam}
                    <br></br>
                    Position: {player.strPosition}
                    <br></br>
                    Player Height: {player.strHeight}
                    <br></br>
                    Player Weight: {player.strWeight}
                    <br></br>
                    About Player: <b>{player.strDescriptionEN}</b>
                  </Card.Text>
                </Card>
          </center>
          </div>
        </Container>
  }
      </Container>
    );
  } 

export default PlayerDetail;