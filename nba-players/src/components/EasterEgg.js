import { Modal, Card } from 'react-bootstrap';
import { useState } from "react";
function EasterEgg () {
    const [showModal, setShowModal] = useState(false);
    const [inputValue, setInputValue] = useState('');
    const [answer, setAnswer] = useState("")

const handleOpenModal = () => {
  setShowModal(true);
};

const handleCloseModal = () => {
  setShowModal(false);
};

const handleChange = (event) => {
    // making input value always lowercase to compare on line 38
    setInputValue(event.target.value.toLowerCase());
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    setAnswer(inputValue);
  }
    return(
        <div>
        <img onClick={handleOpenModal} src='RBall.png' alt="RBall" width='7rem' ></img>        
        <Modal show={showModal} onHide={handleCloseModal} title="My Modal">
        <Card title="NBA Questions">
          <p>What Player In The NBA Has The Record For Most 3 Pointers In One Game</p>
          <form onSubmit={handleSubmit} align="center">
            <label >
                 Answer:
                <input type="text" value={inputValue} onChange={handleChange} />
            </label>
            <input type="submit" value="Submit" />
        </form>
        {/* checks answer to see if it is equal to klay thompson in lowercase */}
        {answer === "klay thompson" ?
        <div align="center"><b>Thats Correct</b><br></br>
        <img src="Klay.jpeg" alt="Klay"/></div>
        
        :
        <img src="questionMark.png" alt="question"/>
    }
          
        </Card>
      </Modal>
    </div>
    )
}
export default EasterEgg