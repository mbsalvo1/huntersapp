import React from "react";
import { Container } from "react-bootstrap";
import EasterEgg from "./EasterEgg";

function Home() {
  return (
    <Container fluid
      style={{
        backgroundImage: `url(https://images.pexels.com/photos/2277981/pexels-photo-2277981.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1d)`,
        backgroundRepeat: "no-repeat",
        backgroundPositionY: "center",
        backgroundSize: "cover",
        height: "100vh",
      }}
    >
      {/* All text and sport logos for home page */}
      <h1 align="center" className="text-white fw-bold" >
        NBA All The Way To WWE
      </h1>

      <h1 align="center" className="text-white fw-bold">
        All Sports Page
      </h1>

      <div align="center">
       <a href="https://www.nba.com/games"><img src="/nba-logo.png" width="60rem" alt="NBA" /></a>
      </div>

      <div align="center">
        <a href="https://www.nfl.com/schedules/"><img src="/NFL-Logo.png" width="75rem" alt="NFL" /></a>
      </div>

      <div align="center">
        <a href="https://www.fifa.com/fifaplus/en/tournaments/mens/"><img src="/FIFA.png" width="90rem" alt="FIFA" /></a>
      </div>

      <div align="center">
        <a href="https://www.mlb.com/schedule/2023-02-24"><img src="/MLB.png" width="90rem" alt="MLB" /></a>
    </div>

    <div align="center">
        <a href="https://www.pgatour.com/tournaments/schedule.html"><img src="PGA.png" width="90rem" alt="PGA" /></a>
    </div>

    <div align="center">
       <a href="https://www.ufc.com/events"><img src="/Logo-UFC.png" width="90rem" alt="UFC" /></a>
    </div>

    <div align="center">
        <a href="https://watch.wwe.com/schedule"><img src="WWE.png" width="70rem" alt="WWE" /></a>
    </div>
    <>
    <EasterEgg/>
    </>
    </Container>
  );
}

export default Home;
